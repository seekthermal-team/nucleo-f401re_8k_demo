# nucleo-f401re_8k_demo

[Seek Thermal](https://www.thermal.com) 8K SPI Core Demo based on [STM32 NUCLEO64 F401RE Board](https://www.st.com/en/evaluation-tools/nucleo-f401re.html)

Builds with free AC6 IDE [System Workbench for STM32](http://www.openstm32.org/HomePage)
