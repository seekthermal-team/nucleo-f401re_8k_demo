#include <stdbool.h>
#include "stm32f4xx_hal.h"
#include "main.h"
#include "seesaw.h"
#include "SPI_core.h"
#include "printf.h"

/* USER CODE BEGIN Includes */
/* vim: set ai et ts=4 sw=4: */
#include <string.h>
#include "SPI_core.h"
#include "st7735/st7735.h"
#include "st7735/fonts.h"

//#define TEST_LCD
bool		bLandscape = true;
bool		bCelsius = true;
LUT_INDEX	colorLUT = SEEK_LUT_DEFAULT;
uint32_t	button2Down;

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi3;

DMA_HandleTypeDef hdma_spi1_tx;
DMA_HandleTypeDef hdma_spi2_rx;
DMA_HandleTypeDef hdma_spi3_rx;
DMA_HandleTypeDef hdma_spi3_tx;


I2C_HandleTypeDef hi2c1;


UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint32_t button_last;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_DMA_Init(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI3_Init(void);
static void MX_I2C1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

void HAL_UART_TransmitChar(char ch)
{
	HAL_UART_Transmit(&huart2, (uint8_t*) &ch, 1, HAL_MAX_DELAY);
}

void WriteLine(uint16_t x, uint16_t y, const char* str, const FontDef font, uint16_t fore, uint16_t back)
{
	if (bLandscape) {
		ST7735_WriteLine(x, y, str, font, fore, back);
	} else {
		ST7735_WriteLine90(x, y, str, font, fore, back);
	}
}

void ShowSplash(uint16_t msec) {
	// Show Splash Bitmap
	if (bLandscape) {
		ST7735_DrawBitmap(0, 0, Seek_160x128_bmp);
	} else {
		ST7735_DrawBitmap(0, 0, Seek_128x160_bmp);
	}
    HAL_Delay(msec);
    // Clear screen
    ST7735_FillScreen(ST7735_BLACK);
}

void init() {
	// Wait for SeeSaw to Boot
	HAL_Delay(100);

	// First reset the LCD display
	SeeSaw_LCD_Reset();

	// Then initialize the TFT Display driver
    ST7735_Init();

    // Then turn on the LED BackLight
    SeeSaw_LCD_BackLight(SEESAW_BACKLIGHT_ON);

    button_last = 0;
    button2Down = HAL_GetTick();

    // Setup DMA to receive frame
	SPI_Capture(&SPI_frame);

	// then wakeup the Core
	Seek_PowerUp();

    ShowSplash(1000);
    SPI_CreateRamp(&SPI_frame);

#ifdef TEST_LCD
    int16_t x = ST7735_WIDTH / 2;
    int16_t y = ST7735_HEIGHT / 2;
    // Check fonts
    ST7735_FillScreen(ST7735_BLACK);
    ST7735_WriteLine(x, 5, "Font_7x10, red, ", Font_7x10, ST7735_RED, ST7735_BLACK);
    ST7735_WriteLine(x, 15, "lorem ipsum dolor", Font_7x10, ST7735_RED, ST7735_BLACK);
    ST7735_WriteLine(x, 35, "Font_11x18", Font_11x18, ST7735_GREEN, ST7735_BLACK);
    ST7735_WriteLine(x, 53, "green, lorem", Font_11x18, ST7735_GREEN, ST7735_BLACK);
    ST7735_WriteLine(x, 75, "Font_16x26", Font_16x26, ST7735_BLUE, ST7735_BLACK);
    HAL_Delay(1000);

    // Check colors
    ST7735_FillScreen(ST7735_BLACK);
    WriteLine(x, y, "BLACK", Font_11x18, ST7735_WHITE, ST7735_BLACK);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_BLUE);
    WriteLine(x, y, "BLUE", Font_11x18, ST7735_BLACK, ST7735_BLUE);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_RED);
    WriteLine(x, y, "RED", Font_11x18, ST7735_BLACK, ST7735_RED);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_GREEN);
    WriteLine(x, y, "GREEN", Font_11x18, ST7735_BLACK, ST7735_GREEN);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_CYAN);
    WriteLine(x, y, "CYAN", Font_11x18, ST7735_BLACK, ST7735_CYAN);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_MAGENTA);
    WriteLine(x, y, "MAGENTA", Font_11x18, ST7735_BLACK, ST7735_MAGENTA);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_YELLOW);
    WriteLine(x, y, "YELLOW", Font_11x18, ST7735_BLACK, ST7735_YELLOW);
    HAL_Delay(500);

    ST7735_FillScreen(ST7735_WHITE);
    WriteLine(x, y, "WHITE", Font_11x18, ST7735_BLACK, ST7735_WHITE);
    HAL_Delay(500);

	// Check border
    for(int x = 0; x < ST7735_WIDTH; x++) {
        ST7735_DrawPixel(x, 0, ST7735_RED);
        ST7735_DrawPixel(x, ST7735_HEIGHT-1, ST7735_RED);
    }

    for(int y = 0; y < ST7735_HEIGHT; y++) {
        ST7735_DrawPixel(0, y, ST7735_RED);
        ST7735_DrawPixel(ST7735_WIDTH-1, y, ST7735_RED);
    }
#endif
}

// Local factorizations to detect button up & down transitions
int ButtonUp(uint32_t buttons, uint32_t mask) {
	return (buttons & mask) == 0 && (button_last & mask) != 0;
}

int ButtonDown(uint32_t buttons, uint32_t mask) {
	return (buttons & mask) != 0 && (button_last & mask) == 0;
}

int16_t roundDegx20(int16_t degx20)
{
	int deg = (degx20 >= 0) ? (degx20 + 10) / 20 : (degx20 - 10) / 20;
	// Clamp to range -999..9999 so display width <= 4
	if (deg < -999) {
		deg = -999;
	} else if (deg > 9999) {
		deg = 9999;
	}
	return deg;
}

// Local factorization to format temp 0.1 degC in Celsius or Fahrenheight
size_t FormatTemp(char* buf, const char* szFormat, int16_t degCx20)
{
	size_t len;
	if (bCelsius) {
		len = tfp_sprintf(buf, szFormat, roundDegx20(degCx20));
	} else {
		int16_t degFx20 = (degCx20 * 9) / 5 + 320;
		len = tfp_sprintf(buf, szFormat, roundDegx20(degFx20));
	}
	return len;
}


//
//	sprintf format strings
//
const char szFormatLo[] = "Lo:%4d";
const char szFormatHi[] = "Hi:%4d";
#define FORMAT_COLD_LEN		9
#define FORMAT_HOT_LEN		8

const char szFormatSpotC[] = "%4d C";
const char szFormatSpotF[] = "%4d F";
#define FORMAT_SPOT_LEN		4

#define LAND_MARGIN_X	((ST7735_WIDTH - FPA_COLS) / 2)
#define LAND_MARGIN_Y	((ST7735_HEIGHT - FPA_ROWS) / 2)

#define TEXT_WIDTH		11
#define TEXT_HEIGHT		18

#define TEXT_COLD_COLOR		ST7735_BLUE
#define TEXT_HOT_COLOR		ST7735_RED
#define TEXT_SPOT_COLOR		ST7735_WHITE
#define TEXT_BACK_COLOR		ST7735_BLACK

//
// 0,0															     W-1,0
//	 ___________________________________________________________________
//	|																	|
//	|								SPOT								|
//	|		 ___________________________________________________		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|													|		|
//	|		|___________________________________________________|		|
//	|																	|
//	|		 Cold: XXXX C							 Hot: XXXX C		|
//	|																	|
//	|___________________________________________________________________|
// 0,H-1															 W-1,H-1
//
#define TEXT_LAND_Y		(ST7735_HEIGHT - LAND_MARGIN_Y / 2)
#define COLD_LAND_X		(LAND_MARGIN_X + FORMAT_COLD_LEN / 2)
#define HOT_LAND_X		(ST7735_WIDTH - LAND_MARGIN_X)
#define SPOT_LAND_X		(ST7735_WIDTH / 2)
#define SPOT_LAND_Y		(LAND_MARGIN_Y / 2)

void landscape(int16_t spot, int16_t cold, int16_t hot) {
	char text[10];
	const char* szFormatSpot = bCelsius ? szFormatSpotC : szFormatSpotF;

	FormatTemp(text, szFormatSpot, spot);
	ST7735_WriteLine(SPOT_LAND_X, SPOT_LAND_Y, text, Font_11x18, TEXT_SPOT_COLOR, TEXT_BACK_COLOR);

	FormatTemp(text, szFormatLo, cold);
	ST7735_WriteLine(COLD_LAND_X, TEXT_LAND_Y, text, Font_7x10, TEXT_COLD_COLOR, TEXT_BACK_COLOR);

	FormatTemp(text, szFormatHi, hot);
	ST7735_WriteLine(HOT_LAND_X, TEXT_LAND_Y, text, Font_7x10, TEXT_HOT_COLOR, TEXT_BACK_COLOR);
}

//
// 0,0															     W-1,0
//	 ___________________________________________________________________
//	|																X	|
//	|																X	|
//	|		 ___________________________________________________	X	|
//	|		|													|	X	|
//	|		|													|	:	|
//	|		|													|	t	|
//	|		|													|	o	|
//	|		|													|	H	|
//	|		|													|		|
//	|	T	|													|		|
//	|	O	|													|		|
//	|	P	|													|		|
//	|	S	|													|		|
//	|		|													|	X	|
//	|		|													|	X	|
//	|		|													|	X	|
//	|		|													|	X	|
//	|		|													|	:	|
//	|		|___________________________________________________|	d	|
//	|																l	|
//	|																o	|
//	|																C	|
//	|___________________________________________________________________|
// 0,H-1															 W-1,H-1
//
#define TEXT_PORT_X		(ST7735_WIDTH - LAND_MARGIN_X / 2)
#define COLD_PORT_Y		(ST7735_HEIGHT * 3 / 4)
#define HOT_PORT_Y		(ST7735_HEIGHT * 1 / 4)

#define SPOT_PORT_X		(LAND_MARGIN_X / 2)
#define SPOT_PORT_Y		(ST7735_HEIGHT / 2)

void portrait(int16_t spot, int16_t cold, int16_t hot) {
	char text[10];
	const char* szFormatSpot = bCelsius ? szFormatSpotC : szFormatSpotF;

	FormatTemp(text, szFormatSpot, spot);
	ST7735_WriteLine90(SPOT_PORT_X, SPOT_PORT_Y, text, Font_11x18, TEXT_SPOT_COLOR, TEXT_BACK_COLOR);

	FormatTemp(text, szFormatLo, cold);
	ST7735_WriteLine90(TEXT_PORT_X, COLD_PORT_Y, text, Font_7x10, TEXT_COLD_COLOR, TEXT_BACK_COLOR);

	FormatTemp(text, szFormatHi, hot);
	ST7735_WriteLine90(TEXT_PORT_X, HOT_PORT_Y, text, Font_7x10, TEXT_HOT_COLOR, TEXT_BACK_COLOR);
}

void loop() {

    uint32_t buttons = SeeSaw_ReadButtons();

	if (ButtonUp(buttons, TFTSHIELD_BUTTON_UP)) {
		HAL_UART_TransmitChar('U');
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_DOWN)) {
		HAL_UART_TransmitChar('D');
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_LEFT)) {
		HAL_UART_TransmitChar('L');
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_RIGHT)) {
		HAL_UART_TransmitChar('R');
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_IN)) {
		HAL_UART_TransmitChar('*');
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_1)) {
		HAL_UART_TransmitChar('1');
		// Previous Color LUT
		if (colorLUT > 0) {
			Seek_SetColorLUT(--colorLUT);
		}
	}
	if (ButtonDown(buttons, TFTSHIELD_BUTTON_2)) {
		button2Down = HAL_GetTick();
	}
	if (buttons & TFTSHIELD_BUTTON_2) {
		// Check if held for more than 1 sec
		if (HAL_GetTick() - button2Down >= 1000) {
			// Toggle Porttrait/Landscape orientation
			bLandscape = !bLandscape;
			ShowSplash(500);
			button2Down = HAL_GetTick();
		}
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_2)) {
		HAL_UART_TransmitChar('2');
		// Toggle Celsius/Fahrenheit
		bCelsius = !bCelsius;
	}
	if (ButtonUp(buttons, TFTSHIELD_BUTTON_3)) {
		HAL_UART_TransmitChar('3');
		// Next Color LUT
		if (colorLUT < COLOR_LUTS - 1) {
			Seek_SetColorLUT(++colorLUT);
		}
	}
	button_last = buttons;
	HAL_UART_TransmitChar('.');
    HAL_Delay(50);

	// Draw FPA image (landscape)
    // Center (for now)
    uint16_t* pixels = SPI_frame.data;
	ST7735_DrawImage((160 - FPA_COLS) / 2, (128 - FPA_ROWS) / 2, FPA_COLS, FPA_ROWS, pixels);

	SPI_TRAILER* trailer = &SPI_frame.trailer;
	if (bLandscape) {
		landscape(trailer->spot, trailer->cold, trailer->hot);
	} else {
		portrait(trailer->spot, trailer->cold, trailer->hot);
	}
}

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  MX_SPI2_Init();
  MX_SPI3_Init();
  MX_I2C1_Init();

  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  init();
  while (1)
  {
    loop();
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI2 init function */
static void MX_SPI2_Init(void)
{

  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_SLAVE;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_INPUT;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI3 init function */
static void MX_SPI3_Init(void)
{

  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
  /* DMA2_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin C7: Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin A9: Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin B6: Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pins : PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function formats an error message & outputs to UART2
  * @param  None
  * @retval None
  */
void ReportError(const char *format, int error)
{
	char errbuf[80];
	int len = tfp_sprintf(errbuf, format, error);
	HAL_UART_Transmit(&huart2, (uint8_t*) errbuf, len, HAL_MAX_DELAY);
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(const char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  char errbuf[80];
  int len = tfp_sprintf(errbuf, "ERROR %s at %%u\r\n", file);
  ReportError(errbuf, line);
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
