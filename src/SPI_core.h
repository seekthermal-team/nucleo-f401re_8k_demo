/*
 * SPI_core.h
 *
 *  Created on: Sep 24, 2018
 *      Author: Lennie Araki
 */

#ifndef __SPI_CORE_H__
#define __SPI_CORE_H__

#include <stm32f4xx_hal_def.h>

#define FPA_ROWS		74
#define FPA_COLS		102

//
//	Copied and renamed from HAL/I2C/i2c.h
//
#define SEEK_I2C_ADDR		0x24
#define SEEK_I2C_TIMEOUT	100				// msec

#define SEEK_RESET_MASK		0x01
#define SEEK_WAKEUP_MASK	0x04

#define SEEK_CTRL_STAT		(16)
#define SEEK_COLORLUT_ADDR	(16+4)			// fro registermapdef.h

//
// Copied from LUTs.h
//
typedef enum {
	LUT_WHITEHOT,
	LUT_COOL,
	LUT_FIRE,
	LUT_GLOW,
	LUT_ICA,
	LUT_SPECTRUM,
	LUT_SMART,
	LUT_UNIONJACK,
	LUT_YELLOWHOT,
	LUT_COLDEST,
	LUT_HOTTEST,
	LUT_REDHOT,
	LUT_SEPIA,
	LUT_ROYAL,
	LUT_JET,
	LUT_GEM,
	LUT_BLACKHOT,
	LUT_PETCOLOR,
	LUT_HOTMETALBLUE,
	LUT_HOTIRON,

	LUT_AMBER,
	LUT_BLACK,
	LUT_HI,
	LUT_HILO,
	LUT_IRON,
	LUT_SPECTRA,
	LUT_PRISM,
	LUT_TYRIAN,
	LUT_WHITE,

	COLOR_LUTS,

	LUT_INDEX_MASK = 255,

	LUT_RGB565 = 0x0000,
	LUT_BGR565 = 0x0100,
	LUT_RGB555 = 0x0200,
	LUT_BGR555 = 0x0300,

} LUT_INDEX;

#define SEEK_LUT_DEFAULT	LUT_IRON

// 16-word "header"
typedef struct SPI_header
{
	uint16_t ROIC_id;		// ROIC Id (057A)
	uint16_t frameNum;		// 16-bit Frame Number
	uint16_t frametype;		// Frame type
	uint16_t tDiode;		// tDiode 16-bit counts (14.2)
	uint16_t testInput;		// TESTB 16-bit counts (14.2)
	uint16_t chipID[3];		// 48-bit chip ID

	uint32_t timestamp;		// TimeStamp (msec)
	uint16_t shutterState;	// Shutter State
	uint16_t colorLUT;		// Color LUT (format, type)
	uint16_t correctMask;	// 16-bit correction mask
	uint16_t errCount;		// Error counter
	uint16_t reserved[2];
} SPI_HEADER;

// 16-word "trailer"
typedef struct SPI_trailer
{
	int16_t spot;			// spot temp (0.05 degC)
	int16_t mean;			// mean scene temp (0.05 degC)
	int16_t cold;			// min temperature (0.05 degC)
	uint16_t cold_X;		// (X,Y) location
	uint16_t cold_y;
	int16_t hot;			// max (hot) temp (0.05 degC)
	uint16_t hot_X;			// (X,Y) location
	uint16_t hot_Y;
	uint16_t reserved[8];
} SPI_TRAILER;

typedef struct spi_frame
{
	SPI_HEADER header;
	uint16_t data[FPA_ROWS * FPA_COLS];
	SPI_TRAILER trailer;
} SPI_FRAME;

extern SPI_FRAME SPI_frame;

extern HAL_StatusTypeDef SPI_Capture(SPI_FRAME* frame);
extern void SPI_CreateRamp(SPI_FRAME* frame);

// Seek I2C Camera Commands
extern HAL_StatusTypeDef Seek_PowerUp(void);
extern HAL_StatusTypeDef Seek_SetColorLUT(LUT_INDEX lut);

#endif /* __SPI_CORE_H__ */
