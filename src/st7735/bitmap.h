#ifndef __BITMAP_H__
#define __BITMAP_H__

/*
 *  Derived from source code at:
 *  http://www.macs.hw.ac.uk/~mjc/wp-mjc-downloads-for-students/Computer_Graphics/Example_11_6/bitmap.h
 */

#include <stdint.h>

/*
 * Bitmap file data structures (these are defined in <wingdi.h> under
 * Windows...)
 *
 * Note that most Windows compilers will pack the following structures, so
 * when reading them under MacOS or UNIX we need to read individual fields
 * to avoid differences in alignment...
 */

#pragma pack(push)
#pragma pack(1)

typedef struct                       /**** BMP file header structure ****/
    {
    uint16_t   bfType;           /* Magic number for file */
    uint32_t   bfSize;           /* Size of file */
    uint16_t   bfReserved1;      /* Reserved */
    uint16_t   bfReserved2;      /* ... */
    uint32_t   bfOffBits;        /* Offset to bitmap data */
    } BITMAPFILEHEADER;

#  define BF_TYPE 0x4D42             /* "MB" */

typedef struct                       /**** BMP file info structure ****/
    {
    uint32_t   biSize;           /* Size of info header */
    int32_t    biWidth;          /* Width of image */
    int32_t    biHeight;         /* Height of image */
    uint16_t   biPlanes;         /* Number of color planes */
    uint16_t   biBitCount;       /* Number of bits per pixel */
    uint32_t   biCompression;    /* Type of compression to use */
    uint32_t   biSizeImage;      /* Size of image data */
    int32_t    biXPelsPerMeter;  /* X pixels per meter */
    int32_t    biYPelsPerMeter;  /* Y pixels per meter */
    uint32_t   biClrUsed;        /* Number of colors used */
    uint32_t   biClrImportant;   /* Number of important colors */
    } BITMAPINFOHEADER;

/*
 * Constants for the biCompression field...
 */

#  define BI_RGB       0             /* No compression - straight BGR data */
#  define BI_RLE8      1             /* 8-bit run-length compression */
#  define BI_RLE4      2             /* 4-bit run-length compression */
#  define BI_BITFIELDS 3             /* RGB bitmap with RGB masks */

typedef struct                       /**** Colormap entry structure ****/
    {
    uint8_t  rgbBlue;          /* Blue value */
    uint8_t  rgbGreen;         /* Green value */
    uint8_t  rgbRed;           /* Red value */
    uint8_t  rgbReserved;      /* Reserved */
    } RGBQUAD;

#pragma pack(pop)

typedef struct
  {
   BITMAPFILEHEADER bmf;
   BITMAPINFOHEADER bmi;
   const RGBQUAD pal[256];
  } BITMAP256;

#endif /* __BITMAP_H__ */

