#include <string.h>
#include "stm32f4xx_hal.h"
#include "st7735.h"
#include "bitmap.h"

#define DELAY 0x80

// based on Adafruit ST7735 library for Arduino
static const uint8_t
  init_cmds1[] = {            // Init for 7735R, part 1 (red or green tab)
    15,                       // 15 commands in list:
    ST7735_SWRESET,   DELAY,  //  1: Software reset, 0 args, w/delay
      150,                    //     150 ms delay
    ST7735_SLPOUT ,   DELAY,  //  2: Out of sleep mode, 0 args, w/delay
      255,                    //     500 ms delay
    ST7735_FRMCTR1, 3      ,  //  3: Frame rate ctrl - normal mode, 3 args:
      0x01, 0x2C, 0x2D,       //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
    ST7735_FRMCTR2, 3      ,  //  4: Frame rate control - idle mode, 3 args:
      0x01, 0x2C, 0x2D,       //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
    ST7735_FRMCTR3, 6      ,  //  5: Frame rate ctrl - partial mode, 6 args:
      0x01, 0x2C, 0x2D,       //     Dot inversion mode
      0x01, 0x2C, 0x2D,       //     Line inversion mode
    ST7735_INVCTR , 1      ,  //  6: Display inversion ctrl, 1 arg, no delay:
      0x07,                   //     No inversion
    ST7735_PWCTR1 , 3      ,  //  7: Power control, 3 args, no delay:
      0xA2,
      0x02,                   //     -4.6V
      0x84,                   //     AUTO mode
    ST7735_PWCTR2 , 1      ,  //  8: Power control, 1 arg, no delay:
      0xC5,                   //     VGH25 = 2.4C VGSEL = -10 VGH = 3 * AVDD
    ST7735_PWCTR3 , 2      ,  //  9: Power control, 2 args, no delay:
      0x0A,                   //     Opamp current small
      0x00,                   //     Boost frequency
    ST7735_PWCTR4 , 2      ,  // 10: Power control, 2 args, no delay:
      0x8A,                   //     BCLK/2, Opamp current small & Medium low
      0x2A,  
    ST7735_PWCTR5 , 2      ,  // 11: Power control, 2 args, no delay:
      0x8A, 0xEE,
    ST7735_VMCTR1 , 1      ,  // 12: Power control, 1 arg, no delay:
      0x0E,
    ST7735_INVOFF , 0      ,  // 13: Don't invert display, no args, no delay
    ST7735_MADCTL , 1      ,  // 14: Memory access control (directions), 1 arg:
      ST7735_ROTATION,        //     row addr/col addr, bottom to top refresh
    ST7735_COLMOD , 1      ,  // 15: set color mode, 1 arg, no delay:
      0x05 },                 //     16-bit color

#if (defined(ST7735_IS_128X128) || defined(ST7735_IS_160X128))
  init_cmds2[] = {            // Init for 7735R, part 2 (1.44" display)
    2,                        //  2 commands in list:
    ST7735_CASET  , 4      ,  //  1: Column addr set, 4 args, no delay:
      0x00, 0x00,             //     XSTART = 0
      0x00, 0x7F,             //     XEND = 127
    ST7735_RASET  , 4      ,  //  2: Row addr set, 4 args, no delay:
      0x00, 0x00,             //     XSTART = 0
      0x00, 0x7F },           //     XEND = 127
#endif // ST7735_IS_128X128

#ifdef ST7735_IS_160X80
  init_cmds2[] = {            // Init for 7735S, part 2 (160x80 display)
    3,                        //  3 commands in list:
    ST7735_CASET  , 4      ,  //  1: Column addr set, 4 args, no delay:
      0x00, 0x00,             //     XSTART = 0
      0x00, 0x4F,             //     XEND = 79
    ST7735_RASET  , 4      ,  //  2: Row addr set, 4 args, no delay:
      0x00, 0x00,             //     XSTART = 0
      0x00, 0x9F ,            //     XEND = 159
    ST7735_INVON, 0 },        //  3: Invert colors
#endif

  init_cmds3[] = {            // Init for 7735R, part 3 (red or green tab)
    4,                        //  4 commands in list:
    ST7735_GMCTRP1, 16      , //  1: Magical unicorn dust, 16 args, no delay:
      0x02, 0x1c, 0x07, 0x12,
      0x37, 0x32, 0x29, 0x2d,
      0x29, 0x25, 0x2B, 0x39,
      0x00, 0x01, 0x03, 0x10,
    ST7735_GMCTRN1, 16      , //  2: Sparkles and rainbows, 16 args, no delay:
      0x03, 0x1d, 0x07, 0x06,
      0x2E, 0x2C, 0x29, 0x2D,
      0x2E, 0x2E, 0x37, 0x3F,
      0x00, 0x00, 0x02, 0x10,
    ST7735_NORON  ,    DELAY, //  3: Normal display on, no args, w/delay
      10,                     //     10 ms delay
    ST7735_DISPON ,    DELAY, //  4: Main screen turn on, no args w/delay
      100 };                  //     100 ms delay

static void ST7735_Select() {
    HAL_GPIO_WritePin(ST7735_CS_GPIO_Port, ST7735_CS_Pin, GPIO_PIN_RESET);
}

void ST7735_Unselect() {
    HAL_GPIO_WritePin(ST7735_CS_GPIO_Port, ST7735_CS_Pin, GPIO_PIN_SET);
}

static void ST7735_Reset() {
    HAL_GPIO_WritePin(ST7735_RES_GPIO_Port, ST7735_RES_Pin, GPIO_PIN_RESET);
    HAL_Delay(5);
    HAL_GPIO_WritePin(ST7735_RES_GPIO_Port, ST7735_RES_Pin, GPIO_PIN_SET);
}

static void ST7735_WriteCommand(uint8_t cmd) {
    HAL_GPIO_WritePin(ST7735_DC_GPIO_Port, ST7735_DC_Pin, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&ST7735_SPI_PORT, &cmd, sizeof(cmd), HAL_MAX_DELAY);
}

static void ST7735_WriteData(uint8_t* buff, size_t buff_size) {
    HAL_GPIO_WritePin(ST7735_DC_GPIO_Port, ST7735_DC_Pin, GPIO_PIN_SET);
    HAL_SPI_Transmit(&ST7735_SPI_PORT, buff, buff_size, HAL_MAX_DELAY);
}

static void ST7735_WritePixels(const uint16_t* buff, size_t count) {
	ST7735_SPI_PORT.Init.DataSize = SPI_DATASIZE_16BIT;
    HAL_GPIO_WritePin(ST7735_DC_GPIO_Port, ST7735_DC_Pin, GPIO_PIN_SET);
    HAL_SPI_Transmit(&ST7735_SPI_PORT, (uint8_t*) buff, count * sizeof(uint16_t), HAL_MAX_DELAY);
	ST7735_SPI_PORT.Init.DataSize = SPI_DATASIZE_8BIT;
}

static void ST7735_ExecuteCommandList(const uint8_t *addr) {
    uint8_t numCommands, numArgs;
    uint16_t ms;

    numCommands = *addr++;
    while(numCommands--) {
        uint8_t cmd = *addr++;
        ST7735_WriteCommand(cmd);

        numArgs = *addr++;
        // If high bit set, delay follows args
        ms = numArgs & DELAY;
        numArgs &= ~DELAY;
        if(numArgs) {
            ST7735_WriteData((uint8_t*)addr, numArgs);
            addr += numArgs;
        }

        if(ms) {
            ms = *addr++;
            if(ms == 255) ms = 500;
            HAL_Delay(ms);
        }
    }
}

static void ST7735_SetAddressWindow(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1) {
    // column address set
    ST7735_WriteCommand(ST7735_CASET);
    uint8_t data[] = { 0x00, x0 + ST7735_XSTART, 0x00, x1 + ST7735_XSTART };
    ST7735_WriteData(data, sizeof(data));

    // row address set
    ST7735_WriteCommand(ST7735_RASET);
    data[1] = y0 + ST7735_YSTART;
    data[3] = y1 + ST7735_YSTART;
    ST7735_WriteData(data, sizeof(data));

    // write to RAM
    ST7735_WriteCommand(ST7735_RAMWR);
}

void ST7735_Init() {
    ST7735_Select();
    ST7735_Reset();
    ST7735_ExecuteCommandList(init_cmds1);
    ST7735_ExecuteCommandList(init_cmds2);
    ST7735_ExecuteCommandList(init_cmds3);
    ST7735_Unselect();
}

void ST7735_DrawPixel(uint16_t x, uint16_t y, uint16_t color) {
    if((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT))
        return;

    ST7735_Select();

    ST7735_SetAddressWindow(x, y, x+1, y+1);
    uint8_t data[] = { color >> 8, color & 0xFF };
    ST7735_WriteData(data, sizeof(data));

    ST7735_Unselect();
}

#define MAX_CHAR_SEGMENT	32

// Draw Character (centered) at x,y
static void ST7735_WriteChar(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor) {
    uint32_t i, b, j;

    uint16_t w = font.width;
    uint16_t h = font.height;

    // Offset x,y to center character
    uint16_t x1 = x - w / 2;
    uint16_t y1 = y - h / 2;

    ST7735_SetAddressWindow(x1, y1, x1+w-1, y1+h-1);
    uint16_t pixel_data[MAX_CHAR_SEGMENT];

    for(i = 0; i < font.height; i++) {
        b = font.data[(ch - 32) * font.height + i];
        for(j = 0; j < font.width; j++) {
            if((b << j) & 0x8000)  {
                pixel_data[j] = color;
            } else {
            	pixel_data[j] = bgcolor;
            }
        }
        // Write a row at a time
        ST7735_WritePixels(pixel_data, font.width);
    }
}

// Draw Character centered at x,y rotated 90 degrees CCW (upwards)
static void ST7735_WriteChar90(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor) {
    uint32_t i, b, j;

    uint16_t w = font.width;
    uint16_t h = font.height;

    // Offset x,y to center character
    uint16_t x1 = x - w / 2;
    uint16_t y1 = y - h / 2;

    ST7735_SetAddressWindow(x1, y1, x1+h-1, y1+w-1);
    uint16_t pixel_data[MAX_CHAR_SEGMENT];

    for(i = font.width; i > 0; --i) {
    	uint32_t mask = 0x8000 >> (i - 1);
        for(j = 0; j < font.height; j++) {
            b = font.data[(ch - 32) * font.height + j];
            if (b & mask)  {
                pixel_data[j] = color;
            } else {
            	pixel_data[j] = bgcolor;
            }
        }
        // Write a column at a time
        ST7735_WritePixels(pixel_data, font.height);
    }
}

// Draw String centered at x,y
void ST7735_WriteLine(uint16_t x, uint16_t y, const char* str, const FontDef font, uint16_t color, uint16_t bgcolor) {
    const size_t len = strlen(str);
    const uint16_t width = len * font.width;

    ST7735_Select();

    // Adjust starting x to center string horizontally
    x -= width / 2;

    for (int i = 0; i < len; ++i) {
        ST7735_WriteChar(x, y, str[i], font, color, bgcolor);
        x += font.width;
    }
    ST7735_Unselect();
}

// Draw String centered at x,y rotated 90 degrees CCW (upwards)
void ST7735_WriteLine90(uint16_t x, uint16_t y, const char* str, const FontDef font, uint16_t color, uint16_t bgcolor) {
    const size_t len = strlen(str);
    const uint16_t width = len * font.width;

    // Adjust starting y to center string vertically
    y += width / 2;

    ST7735_Select();
    for (int i = 0; i < len; ++i) {
        ST7735_WriteChar90(x, y, str[i], font, color, bgcolor);
        y -= font.width;
    }

    ST7735_Unselect();
}

void ST7735_FillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t color) {
    // clipping
    if((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT)) return;
    if((x + w - 1) >= ST7735_WIDTH) w = ST7735_WIDTH - x;
    if((y + h - 1) >= ST7735_HEIGHT) h = ST7735_HEIGHT - y;

    ST7735_Select();
    ST7735_SetAddressWindow(x, y, x+w-1, y+h-1);

    HAL_GPIO_WritePin(ST7735_DC_GPIO_Port, ST7735_DC_Pin, GPIO_PIN_SET);

	ST7735_SPI_PORT.Init.DataSize = SPI_DATASIZE_16BIT;
    for(y = h; y > 0; y--) {
        for(x = w; x > 0; x--) {
            HAL_SPI_Transmit(&ST7735_SPI_PORT, (uint8_t*) &color, sizeof(color), HAL_MAX_DELAY);
        }
    }
	ST7735_SPI_PORT.Init.DataSize = SPI_DATASIZE_8BIT;

    ST7735_Unselect();
}

void ST7735_FillScreen(const uint16_t color) {
    ST7735_FillRectangle(0, 0, ST7735_WIDTH, ST7735_HEIGHT, color);
}

void ST7735_DrawImage(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t* data) {
    if((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT)) return;
    if((x + w - 1) >= ST7735_WIDTH) return;
    if((y + h - 1) >= ST7735_HEIGHT) return;

    ST7735_Select();
    ST7735_SetAddressWindow(x, y, x+w-1, y+h-1);
    ST7735_WritePixels(data, w*h);
    ST7735_Unselect();
}

uint16_t ST7735_DrawBitmap(uint16_t x, uint16_t y, const uint8_t* data) {
	const BITMAP256* bitmap = (const BITMAP256*) data;

	uint32_t bmpImageoffset;        // Start of image data in file
	uint32_t rowSize;               // Not always = bmpWidth; may have padding
	uint8_t goodBmp = false;       // Set to true on valid header parse
	uint8_t flip = true;        // BMP is stored bottom-to-top
	uint16_t row, col;
	uint32_t pos = 0;

	if ((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT))
		return false;

	// Parse BMP header
	if (bitmap->bmf.bfType == 0x4D42) { // BMP signature
		bmpImageoffset = bitmap->bmf.bfOffBits;
		// Read DIB header
		int bmpWidth = bitmap->bmi.biWidth;
		int bmpHeight = bitmap->bmi.biHeight;
		if (bitmap->bmi.biPlanes == 1) { // # planes -- must be '1'
			uint16_t bmpDepth = bitmap->bmi.biBitCount; // bits per pixel
			if ((bmpDepth == 8) && (bitmap->bmi.biCompression == 0)) { // 0 = uncompressed

				goodBmp = true; // Supported BMP format -- proceed!

				// BMP rows are padded (if needed) to 4-byte boundary
				rowSize = (bmpWidth + 3) & ~3;

				// If bmpHeight is negative, image is in top-down order.
				// This is not canon but has been observed in the wild.
				if (bmpHeight < 0) {
					bmpHeight = -bmpHeight;
					flip = false;
				}

				// Crop area to be loaded
				int w = bmpWidth;
				int h = bmpHeight;
				if ((x + w - 1) >= ST7735_WIDTH)
					w = ST7735_WIDTH - x;
				if ((y + h - 1) >= ST7735_HEIGHT)
					h = ST7735_HEIGHT - y;

				ST7735_Select();

				// Set TFT address window to clipped image bounds
				ST7735_SetAddressWindow(x, y, x+w-1, y+h-1);

				for (row = 0; row < h; row++) { // For each scanline...

					// Seek to start of scan line.  It might seem labor-
					// intensive to be doing this on every line, but this
					// method covers a lot of gritty details like cropping
					// and scanline padding.  Also, the seek only takes
					// place if the file position actually needs to change
					// (avoids a lot of cluster math in SD library).
					if (flip) // Bitmap is stored bottom-to-top order (normal BMP)
						pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
					else
						// Bitmap is stored top-to-bottom
						pos = bmpImageoffset + row * rowSize;

					uint8_t* pixels = (uint8_t*) bitmap + pos;

					uint16_t line[ST7735_WIDTH];
					for (col = 0; col < w; col += 2) { // For each pair of pixels...
						uint16_t p1p2 = *(uint16_t*) (pixels + col);
						RGBQUAD rgb;
						rgb = bitmap->pal[p1p2 & 255];
						line[col] = ST7735_COLOR565(rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
						rgb = bitmap->pal[p1p2 >> 8];
						line[col + 1] = ST7735_COLOR565(rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
					} // end col
					ST7735_WritePixels(line, w);
				} // end scanline
				ST7735_Unselect();
			} // end goodBmp
		}
	}
	return goodBmp;
}

void ST7735_InvertColors(bool invert) {
    ST7735_Select();
    ST7735_WriteCommand(invert ? ST7735_INVON : ST7735_INVOFF);
    ST7735_Unselect();
}


