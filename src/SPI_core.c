/*
 * SPI_core.c
 *
 *  Created on: Sep 24, 2018
 *      Author: Lennie Araki
 */

#include "stm32f4xx_hal.h"
#include "main.h"
#include "st7735/st7735.h"
#include "SPI_core.h"

// SPI Frame Buffer: either raw 16-bit Radiometric data or RGB565 colorized pixels
SPI_FRAME SPI_frame;

#define SPI_EXTRA_SIZE	(sizeof(SPI_HEADER) + sizeof(SPI_TRAILER))

HAL_StatusTypeDef SPI_Capture(SPI_FRAME* frame)
{
	size_t count = FPA_ROWS * FPA_COLS + SPI_EXTRA_SIZE / sizeof(uint16_t);
	// Receive the SPI Frame in one chunk using DMA!
	HAL_StatusTypeDef status;
	status = HAL_SPI_Receive_DMA(&hspi2, (uint8_t*) frame, count);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	}
	return status;
}

void SPI_CreateRamp(SPI_FRAME* frame) {
	// Setup default temps (for debugging)
	frame->trailer.spot = 200*2;
	frame->trailer.cold = -100*2;
	frame->trailer.hot = 9990*2;
    // Fill with a 12-bit ramp pattern
    uint16_t u;
    for (u = 0; u < FPA_ROWS * FPA_COLS; ++u) {
		uint8_t r = (u & 0xF00) >> 4;
		uint8_t g = (u & 0xF0);
		uint8_t b = (u & 0x0F) << 4;
		frame->data[u] = ST7735_COLOR565(r, g, b);
    }
}

HAL_StatusTypeDef Seek_PowerUp(void) {

	HAL_StatusTypeDef status;
	uint8_t cmd[2];

	// Set lut in native Little Endian format
	cmd[0] = SEEK_CTRL_STAT;
	cmd[1] = SEEK_WAKEUP_MASK;

	// Reset the LCD
	status = HAL_I2C_Master_Transmit(&hi2c1, SEEK_I2C_ADDR * 2, cmd, sizeof(cmd), SEEK_I2C_TIMEOUT);
	if (status != HAL_OK) {
		ReportError("PowerUp FAIL=%d\n", status);
	}
	return status;
}


HAL_StatusTypeDef Seek_SetColorLUT(LUT_INDEX lut) {

	HAL_StatusTypeDef status;
	uint8_t cmd[3];

	// Set lut in native Little Endian format
	cmd[0] = SEEK_COLORLUT_ADDR;
	cmd[1] = (uint8_t) (lut);
	cmd[2] = (uint8_t) (lut >> 8);

	// Reset the LCD
	status = HAL_I2C_Master_Transmit(&hi2c1, SEEK_I2C_ADDR * 2, cmd, sizeof(cmd), SEEK_I2C_TIMEOUT);
	if (status != HAL_OK) {
		ReportError("SetColorLUT FAIL=%d\n", status);
	}
	return status;
}
