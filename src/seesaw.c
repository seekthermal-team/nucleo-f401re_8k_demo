/*
 * seesaw.c
 *
 *  Created on: Sep 24, 2018
 *      Author: Lennie Araki
 */
#include "stm32f4xx_hal.h"
#include "main.h"
#include "seesaw.h"

#define TFTSHIELD_ADDR		0x2E
#define I2C_TIMEOUT			100			/* msec */

#define SEESAW_GPIO_DIRSET_BULK	0x0102
#define SEESAW_GPIO_DIRCLR_BULK	0x0103
#define SEESAW_GPIO_BULK		0x0104
#define SEESAW_GPIO_BULK_SET	0x0105
#define SEESAW_GPIO_BULK_CLR	0x0106

#define SEESAW_GPIO_PULLENSET	0x010B
#define SEESAW_GPIO_PULLENCLR	0x010C

#define SEESAW_LCDRESET		0x00

#define TFTSHIELD_RESET_PIN		3
#define TFTSHIELD_RESET_MASK	(1UL << TFTSHIELD_RESET_PIN)

const uint8_t LCD_Reset[] = { SEESAW_LCDRESET, 0x01 };

void SeeSaw_LCD_Reset(void) {
	HAL_StatusTypeDef status;

	// Reset the LCD
	status = HAL_I2C_Master_Transmit(&hi2c1, TFTSHIELD_ADDR * 2, (uint8_t*) LCD_Reset, sizeof(LCD_Reset), I2C_TIMEOUT);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	}
	HAL_Delay(100);
	// Set all buttons to input pins
	uint8_t ipins[] = {
			(uint8_t) (TFTSHIELD_BUTTON_ALL >> 24),
			(uint8_t) (TFTSHIELD_BUTTON_ALL >> 16),
			(uint8_t) (TFTSHIELD_BUTTON_ALL >> 8),
			(uint8_t) TFTSHIELD_BUTTON_ALL
	};
	status = HAL_I2C_Mem_Write(&hi2c1, TFTSHIELD_ADDR * 2, SEESAW_GPIO_DIRCLR_BULK, 2, ipins, 4, I2C_TIMEOUT);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	}
	status = HAL_I2C_Mem_Write(&hi2c1, TFTSHIELD_ADDR * 2, SEESAW_GPIO_PULLENSET, 2, ipins, 4, I2C_TIMEOUT);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	}
	status = HAL_I2C_Mem_Write(&hi2c1, TFTSHIELD_ADDR * 2, SEESAW_GPIO_BULK_SET, 2, ipins, 4, I2C_TIMEOUT);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	}
}

#define BACKLIGHT_ADDR		0x0801
#define SEESAW_BACKLIGHT	0x00

#define BULKREAD_ADDR		0x0104

void SeeSaw_LCD_BackLight(const uint16_t value) {
	uint8_t cmd[3];
	cmd[0] = SEESAW_BACKLIGHT;
	cmd[1] = (uint8_t) (value >> 8);
	cmd[2] = (uint8_t) value;
	HAL_StatusTypeDef status;
	// Set the LED Backlight register
	status = HAL_I2C_Mem_Write(&hi2c1, TFTSHIELD_ADDR * 2, BACKLIGHT_ADDR, 2, cmd, sizeof(cmd), I2C_TIMEOUT);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	}
}

uint32_t SeeSaw_ReadButtons(void) {
	uint8_t buf[4];
	HAL_StatusTypeDef status;
	status = HAL_I2C_Mem_Read(&hi2c1, TFTSHIELD_ADDR * 2, BULKREAD_ADDR, 2, buf, sizeof(buf), I2C_TIMEOUT);
	if (status != HAL_OK) {
	    _Error_Handler(__FILE__, __LINE__);
	    return 0;
	}
	// Return button inverted as hardware is active low
	uint32_t buttons = ((uint32_t)buf[0] << 24) | ((uint32_t)buf[1] << 16) | ((uint32_t)buf[2] << 8) | (uint32_t)buf[3];
	return ~buttons & TFTSHIELD_BUTTON_ALL;
}
